package com.huawei.servicecomb.controller;



import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

public class TestProjectg08d {

        Projectg08dDelegate projectg08dDelegate = new Projectg08dDelegate();


    @Test
    public void testhelloworld(){

        String expactReturnValue = "hello"; // You should put the expect String type value here.

        String returnValue = projectg08dDelegate.helloworld("hello");

        assertEquals(expactReturnValue, returnValue);
    }

}