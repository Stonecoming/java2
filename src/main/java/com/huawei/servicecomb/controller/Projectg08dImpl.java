package com.huawei.servicecomb.controller;


import javax.ws.rs.core.MediaType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import org.apache.servicecomb.provider.rest.common.RestSchema;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.CseSpringDemoCodegen", date = "2019-10-30T07:47:18.613Z")

@RestSchema(schemaId = "projectg08d")
@RequestMapping(path = "/rest", produces = MediaType.APPLICATION_JSON)
public class Projectg08dImpl {

    @Autowired
    private Projectg08dDelegate userProjectg08dDelegate;


    @RequestMapping(value = "/helloworld",
        produces = { "application/json" }, 
        method = RequestMethod.GET)
    public String helloworld( @RequestParam(value = "name", required = true) String name){

        return userProjectg08dDelegate.helloworld(name);
    }

}
